import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { Button, CircularProgress, Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUser, pageChangePagination } from '../actions/user.actions';

const UserList = () => {
    const dispatch = useDispatch();
    const { users, pending, limit, noPage, currentPage } = useSelector((reduxData) => reduxData.userReducer);
    console.log(pending)
    useEffect(() => {
        dispatch(fetchUser(currentPage, limit))
    }, [currentPage])

    const onDetailClickButton = (userInfo) => {
        console.log(userInfo);
    }

    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }
    return (
        <>
            <Container>
                <Grid container mt={5}>
                    {
                        pending ?
                            <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                                <CircularProgress />
                            </Grid>
                            :
                            <Grid item lg={12} md={12} sm={12} xs={12} >
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="center">ID</TableCell>
                                                <TableCell align="left">Name</TableCell>
                                                <TableCell align="left">UserName</TableCell>
                                                <TableCell align="left">Email</TableCell>
                                                <TableCell align="left">Website</TableCell>
                                                <TableCell align="center">Action</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                users.map((user, index) => (
                                                    <TableRow
                                                        key={index}
                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    >
                                                        <TableCell align="center" component="th" scope="row">
                                                            {user.id}
                                                        </TableCell>
                                                        <TableCell align="left">{user.name}</TableCell>
                                                        <TableCell align="left">{user.username}</TableCell>
                                                        <TableCell align="left">{user.email}</TableCell>
                                                        <TableCell align="left">{user.phone}</TableCell>
                                                        <TableCell align="left"><Button onClick={() => onDetailClickButton(user)}>Chi tiết</Button></TableCell>
                                                    </TableRow>
                                                ))
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>


                            </Grid>
                    }
                    <Grid item lg={12} md={12} sm={12} xs={12} mt={4} style={{ display: "flex", justifyContent: "center" }} >
                        <Pagination count={noPage} page={currentPage} onChange={onChangePagination} variant="outlined" shape="rounded" />
                    </Grid>
                </Grid>

            </Container >

        </>
    )
}
export default UserList