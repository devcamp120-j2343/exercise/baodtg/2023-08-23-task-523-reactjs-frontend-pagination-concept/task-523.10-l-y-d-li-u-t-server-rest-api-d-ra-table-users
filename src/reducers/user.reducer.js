import { PAGE_CHANGE_PAGINATION, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constants";

const initialState = {
    users: [],
    pending: false,
    error: '',
    limit: 3,
    noPage: 0,
    currentPage: 1

}
const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_FETCH_PENDING:
            state.pending = true;
            break;

        case USER_FETCH_SUCCESS:
            state.pending = false;
            state.noPage = Math.ceil(action.totalUser / state.limit);

            state.users = action.data;

            break;
        case USER_FETCH_ERROR:
            // Sử dụng SnackBar để hiển thị lỗi
            state.error = action.error
            break;
        case PAGE_CHANGE_PAGINATION:
            state.currentPage = action.payload;
            break;
        default:
            break;
    }

    return { ...state };
}
export default userReducer