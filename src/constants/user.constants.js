export const USER_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy danh sách User";

export const USER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy danh sách User"; 

export const USER_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy danh sách User"; 

export const PAGE_CHANGE_PAGINATION = "Sự kiện thay đổi trang";