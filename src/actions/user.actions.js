import { PAGE_CHANGE_PAGINATION, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constants"

export const fetchUser = (page, limit) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }
            await dispatch({
                type: USER_FETCH_PENDING
            })

            // Gọi để lấy tổng số users có trong CSDL
            const responseUsers = await fetch("https://jsonplaceholder.typicode.com/users", requestOptions);

            const dataUsers = await responseUsers.json();

            const params = new URLSearchParams({
                "_start": (page - 1) * limit,
                "_limit": limit
            })
            console.log(params.toString())

            const responsePaginationUsers = await fetch("https://jsonplaceholder.typicode.com/users?" + params.toString(), requestOptions);

            const dataPagintionUsers = await responsePaginationUsers.json();

            return dispatch({
                type: USER_FETCH_SUCCESS,
                totalUser: dataUsers.length,
                data: dataPagintionUsers

            })



        } catch (error) {
            return dispatch({
                type: USER_FETCH_ERROR,
                error: error
            })
        }
    }
}

export const pageChangePagination = (page) => {
    return {
        type: PAGE_CHANGE_PAGINATION,
        payload: page
    }
}


